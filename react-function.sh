#!/bin/bash

development=false

arg1=$1
arg2=$2
arg3=$3
arg4=$4

if [ $# -eq 0 ]; then
	echo "######################################################################################################################################"
	echo "Script 'react-function' creates functional React component. It can take no less than one and no more than four arguments."
	echo "- First argument should be component path (relative or absolute) including component name at the end."
	echo "  If a directory specified in the path doesn't exist it will be created. If the component already exists it will not be overwritten."
	echo "- Second and third arguments are (order doesn't matter): '-c' or '--css' (used to create a '.module.css' file alongside a '.js' file),"
	echo "  '-d' or '--directory' (used to put component in a separate directory with an identical name as a component name)."
	echo "- Fourth argument can be '-s/--short' (short arrow function notation; default) or '-l/--long' (long arrow function notation)."
	echo "######################################################################################################################################"
elif [ $# -gt 4 ]; then
	echo "Too many arguments! Run 'react-function' for help."
else
	component=$(basename "${arg1%.*}") # component name
	component=${component,} # Lowercase component name
	Component=${component^} # Capitalized component name
	folder=$(dirname $arg1) # component folder
	path=$folder/$Component.js # component path
	cssPath=$folder/$Component.module.css # component style path

	componentFolder=false
	cssFileCreated=false

	if [ ! -z $arg2 ] ; then
		if [ $arg2 = "-d" -o $arg2 = "--directory" ]; then
			if ! $componentFolder ; then
		  		folder=$folder/$Component
		  		path=$folder/$Component.js
		  		cssPath=$folder/$Component.module.css
		  		componentFolder=true
		  	fi
		fi
	fi

	if [ ! -z $arg3 ] ; then
		if [ $arg3 = "-d" -o $arg3 = "--directory" ]; then
			if ! $componentFolder ; then
		  		folder=$folder/$Component
		  		path=$folder/$Component.js
		  		cssPath=$folder/$Component.module.css
		  	fi
		fi
	fi

	if [ ! -f "${path}" ]; then

		if [ ! -d "${folder}" ]; then
		  	mkdir -p ${folder}
		fi

		msg=""

		if [ ! -z $arg2 ] ; then
			if [ $arg2 = "-c" -o $arg2 = "--css" ]; then
			  	touch $cssPath
			  	cssFileCreated=true
			  	msg="Css file '$cssPath' created!"
			fi
		fi

		if [ ! -z $arg3 ] ; then
			if [ $arg3 = "-c" -o $arg3 = "--css" ]; then
			  	touch $cssPath
			  	cssFileCreated=true
			  	msg="Css file '$cssPath' created!"
			fi
		fi

	  	touch $path

	  	if [ ! -z $arg4 ]; then
	  		if [ $arg4 = "-l" -o $arg4 = "--long" ]; then
	  			echo "import React from 'react';" >> $path
	  			if $cssFileCreated ; then
	  				echo "import classes from './$Component.module.css';" >> $path
	  			fi
				echo >> $path
		  		echo "const $component = (props) => {" >> $path
		  		echo >> $path
		  		echo "	return (" >> $path
		  		echo "		<div></div>" >> $path
		  		echo "	);" >> $path
		  		echo "}" >> $path
		  		echo >> $path
				echo "export default $component;" >> $path
		  	fi
	  	else
	  		echo "import React from 'react';" >> $path
			if $cssFileCreated ; then
  				echo "import classes from './$Component.module.css';" >> $path
  			fi
			echo >> $path
			echo "const $component = (props) => (" >> $path
		  	echo "	<div></div>" >> $path
			echo ");" >> $path
			echo >> $path
			echo "export default $component;" >> $path
	  	fi

	echo "Functional component '$path' created! $msg"

	else
		echo "Component '$path' already exists!"
	fi
fi

if $development ; then
	echo "component: $component"
	echo "Component: $Component"
	echo "folder: $folder"
	echo "path: $path"
fi
