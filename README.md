[//]: # (Are you tired of writing your React components from scratch over and over again? Would you, rather than maniacally banging your keyboard to write the same code again and again spend that precious hours doing sexy time with your woman and maniacally banging her instead? Look no further than, because Bash scripts for creating React components are here! :star: :man_dancing::skin-tone-3::star: It's easy as calling a script with only a component name as a parameter. Plus, with a few extra parameters the possibilities for customizing your component are endless! It's a no-brainer! :drooling_face: Fuck yeah!)

## react-function

- Script `react-function` creates functional React component. It can take no less than one and no more than four arguments.
- First argument should be component path (relative or absolute) including component name at the end. If a directory specified in the path doesn't exist it will be created. If the component already exists it will not be overwritten.
- Second and third arguments are (order doesn't matter): `-c` or `--css` (used to create a `.module.css` file alongside a `.js` file), `-d` or `--directory` (used to put component in a separate directory with an identical name as a component name).
- Fourth argument can be `-s/--short` (short arrow function notation; default) or `-l/--long` (long arrow function notation).

## react-class

- Script `react-class` creates React class component. It can take no less than one and no more than three arguments.
- First argument should be component path (relative or absolute) including component name at the end. If a directory specified in the path doesn't exist it will be created. If the component already exists it will not be overwritten.
- Second and third arguments are (order doesn't matter): `-c` or `--css` (used to create a `.module.css` file alongside a `.js` file) and `-d` or `--directory` (used to put component in a separate directory with an identical name as a component name).
